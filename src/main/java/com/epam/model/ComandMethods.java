package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Class shows how to use lambdas and method references.
 * Created by Borys Latyk on 28/11/2019.
 *
 * @version 2.1
 * @since 26.11.2019
 */
public class ComandMethods {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    List<Command> executecommand = new ArrayList<>();

    public void executeBridge(Command command) {
        executecommand.add(command);
        command.execute("Method references");

    }

    public void executRealiz() {
        ComandMethods d = new ComandMethods();
        Commands a = new Commands();
        d.executeBridge(a::command1);

        Command c = (type) -> {
            logger1.info(type);
        };
        c.execute("Lambda");


    }
}