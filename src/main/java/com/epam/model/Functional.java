package com.epam.model;

/**
 * Functional interface to test lambdas.
 * Created by Borys Latyk on 28/11/2019.
 *
 * @version 2.1
 * @since 26.11.2019
 */
@FunctionalInterface
public interface Functional {
    int values(int a, int b, int c);
}
