package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Class to test lambdas expressions.
 * Created by Borys Latyk on 28/11/2019.
 *
 * @version 2.1
 * @since 26.11.2019
 */
public class LambdasLogic {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    static ArrayList<Integer> numbers;

    public static void showMax(int a, int b, int c) {

        Functional functional = (d, e, g) -> {
            int max;
            numbers = new ArrayList<>();
            numbers.add(a);
            numbers.add(b);
            numbers.add(c);
            max = Collections.max(numbers);
            return max;
        };
        logger1.info("Max value is " + functional.values(a, b, c));
    }

    public static void showAverage(int a, int b, int c) {
        Functional functional1 = (d, e, g) -> {
            int average = (d + e + g) / 3;
            return average;
        };
        logger1.info("Average value is " + functional1.values(a, b, c));
    }

}