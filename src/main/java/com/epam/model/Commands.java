package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * Class to test lambdas.
 * Created by Borys Latyk on 28/11/2019.
 *
 * @version 2.1
 * @since 26.11.2019
 */
public class Commands {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    public void command1(String name) {
        logger1.info(name);

    }

}