package com.epam.model;


import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class to read unique words.
 * Created by Borys Latyk on 28/11/2019.
 *
 * @version 2.1
 * @since 26.11.2019
 */
public class TextLine {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    public static void textStream() throws IOException {
        Set<String> stringSet = new HashSet<>();
        String s;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        do {
            logger1.info("Please, type a few Sentences");
            s = reader.readLine();
            if (!s.equals("")) {
                s.split(" ");
                stringSet.add(s);
            }
        } while (!s.equals(""));
        for (String word : stringSet) {
            Stream.of(word.split("[^A-Za-zА-Яа-я]+"))
                    .map(String::toUpperCase)
                    .collect(Collectors.toSet())
                    .forEach(System.out::println);
        }


    }
}
