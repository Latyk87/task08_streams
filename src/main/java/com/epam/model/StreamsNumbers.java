package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Class to test Stream API methods.
 * Created by Borys Latyk on 28/11/2019.
 *
 * @version 2.1
 * @since 26.11.2019
 */
public class StreamsNumbers implements Model {
    private static Logger logger1 = LogManager.getLogger(Application.class);

    @Override
    public void streamsNumbers() {
        Stream<Integer> stream = Stream.of(1, 2, 3, 8, 5, 6, 7, 8, 9, 3, 4, 5, 34, 23, 46);
        logger1.info("Sum is " + stream.mapToInt(x -> x).sum());
        IntStream intStream = IntStream.of(1, 2, 3, 8, 5, 6, 7, 8, 9, 3, 4, 5, 34, 23, 46);
        logger1.info("Average is " + intStream.average());
        IntStream intStream2 = IntStream.of(1, 2, 3, 8, 5, 6, 7, 8, 9, 3, 4, 5, 34, 23, 46);
        logger1.info("Minimum is " + intStream2.min());
        IntStream intStream3 = IntStream.of(1, 2, 3, 8, 5, 6, 7, 8, 9, 3, 4, 5, 34, 23, 46);
        logger1.info("Maximum is " + intStream3.max());
    }

}
